import React from 'react';

const immagini = [
    {
        src: "/immagini/Muse.jpg", 
        alt: "Immagine gruppo", 
        title: "Vieni con noi in Tour!", 
        description: "Acquista i biglietti per il tour europeo!"
    },
    
    { 
        src: "/immagini/merch2.jpg", 
        alt: "Immagine merchandiser", 
        title: "Nuove ristampe in edizione limitata!", 
        description: "Affrettati!" 
    },

    { 
        src: "/immagini/newsletter2.jpg", 
        alt: "Immagine newsletter", 
        title: "Newsletter", 
        description: "Iscriviti alla nostra Newsletter per rimanere sempre aggiornato!" 
    }
];

export default function Carosello() {
    return (
        <>
            {/* Uso il metodo map per mappare ogni immagine nell'array per creare un bottone */}
            <div id="carouselExampleCaptions" className="carousel slide" data-bs-ride="carousel" style={{ fontFamily: 'Georgia, "Times New Roman", Times, serif' }}>
                <div className="carousel-indicators">
                    {immagini.map((Immagine, index) => (
                        <button key={index} type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to={index} className={index === 0 ? "active" : ""} aria-label={`Slide ${index + 1}`}></button>
                    ))}
                </div>
                {/* Uso il metodo map per mappare ogni immagine nell'array per creare stavolta una slide 
                uso un operatore ternario, che permette di aggiungere la classe active al bottone nel caso in cui l'indice sia 0.
                Quindi se l'indice dell'immagine è uguale a 0, significa che il primo elemento dell'array dovrebbe essere attivo e scorrere */}
                <div className="carousel-inner">
                    {immagini.map((immagine, index) => (
                        <div key={index} className={`carousel-item ${index === 0 ? "active" : ""}`}>
                            <img src={immagine.src} className="d-block w-100 img-fluid" alt={immagine.alt} style={{ height: '100%', weight: '100%' }}/>
                            <div className="carousel-caption d-none d-md-block">
                                <h5>{immagine.title}</h5>
                                <p>{immagine.description}</p>
                            </div>
                        </div>
                    ))}
                </div>

                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Next</span>
                </button>
            </div>   
        </>
    );
}
