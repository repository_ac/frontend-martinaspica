import React from "react"

const immaginiTour = [
    {
        src: "/immagini/parigi.jpg",
        alt: "Immagine di Parigi",
        luogo: "Parigi",
        data: "10/09/2024"
    },

    {
        src: "/immagini/newY.png",
        alt: "Immagine di New York",
        luogo: "New York",
        data: "15/10/2024"
    },
    
    {
        src: "/immagini/roma.jpg",
        alt: "Immagine di Roma",
        luogo: "Roma",
        data: "20/11/2024"
    }
];

export default function DataTour(){
    return (
        <section className="tour-datas" style={{ backgroundColor: "black", fontFamily: 'Georgia, "Times New Roman", Times, serif'}}>

            <div className="black-section">
                <h2 style={{ fontStyle: "italic", textAlign: "center" }}>Tour datas</h2>
                <table className="table table-hover" style={{textAlign: "start"}}>
                    <tbody>
                        <tr>
                            <td>Settembre<span style={{ color: "red", fontWeight: "bold", fontStyle: "italic" }}> Sold Out!</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Ottobre <span style={{ color: "red", fontWeight: "bold", fontStyle: "italic" }}> Sold Out!</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Novembre</td>
                        </tr>
                    </tbody>
                </table>

                <div className="card-container">
                    {immaginiTour.map((immagine, index) => (
                        <div className="card" key={index}>
                            <img src={immagine.src} alt={immagine.alt} />
                            <h3>{immagine.luogo}</h3>
                            <p>Data: {immagine.data}</p>
                            <button className="button">Acquista biglietto</button>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    );
}

