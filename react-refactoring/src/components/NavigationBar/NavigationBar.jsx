import React from "react";

const menu = ['HOME', 'BAND', 'TOUR', 'CONTACT'];

const menuTendina = ['Calendario tour', 'Foto Band', 'Contattaci!'];


export default function NavigationBar() {
    return (
        <>
            <nav className="navbar navbar-expand-lg bg-dark fixed-top">
                <div className="container-fluid" style={{ fontFamily: 'Georgia, "Times New Roman", Times, serif' }}>
                    <a className="navbar-brand" href="#" style={{ color: "white" }}>Logo</a>

                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll"
                        aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    {/* <!--barra di navigazione --> */}
                    <div className="collapse navbar-collapse justify-content-end" id="navbarScroll">
                        <ul className="navbar-nav my-2 my-lg-0 navbar-nav-scroll" style={{ "--bs-scroll-height": "100px" }}>

                        {menu.map((item, index) => (
                                <li className="nav-item" key={index}>
                                    <a className="nav-link active" aria-current="page" href="#" style={{ color: "white" }}>{item}</a>
                                </li>
                            ))}

                            {/* <!--Menu a tendina--> */}
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" style={{ color: "white" }}>MORE </a>
                                <ul className="dropdown-menu">
                                    {menuTendina.map((item, index) => (
                                        <li key={index}><a className="dropdown-item" href="#" >{item}</a></li>
                                    ))}
                                </ul>
                            </li>

                            {/* icona lente */}
                            <li className="nav-item">
                                <button className="btn btn-outline-success" type="button" style={{ border: "none" }}><i className="bi bi-search" style={{ color: "white" }}></i></button>
                            </li>

                        </ul>
                    </div>
                </div>
            </nav>
        </>
    );
}
