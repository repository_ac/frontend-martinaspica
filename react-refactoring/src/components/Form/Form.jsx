import React from "react";

const contatti = [
    {
        icon: "fa fa-map-marker",
        label: "Città:",
        value: "Chicago"
    },

    {
        icon: "fa fa-mobile",
        label: "Telefono:",
        value: "+1 (333) 111111"
    },

    {
        icon: "fa fa-envelope",
        label: "Email:",
        value: "contattaci@gmail.com"
    }

];

export default function Form() {
    return (
        <section id="contatti" style={{ padding: "2%", fontFamily: 'Georgia, "Times New Roman", Times, serif'}}>
            <div className="container">
                <h2 style={{ textAlign: "center", letterSpacing: "5px" }}>Contatti</h2>
                <p style={{ textAlign: "center", fontStyle: "italic" }}>Amiamo i nostri fan!</p>
                <div className="contact-section">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="contact-info">
                                <p><strong><i className="fa fa-address-book"></i> Contatti:</strong></p>
                                {contatti.map((contatto, index) => (
                                    <p key={index}><strong><i className={contatto.icon}></i> {contatto.label}</strong> {contatto.value}</p>
                                ))}
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="contact-form">
                                <form action="#" method="post">
                                    <input type="text" name="name" className="form-control" placeholder="Nome" required />
                                    <input type="email" name="email" className="form-control" placeholder="Email" required />
                                    <textarea name="message" className="form-control" placeholder="Messaggio" required></textarea>
                                    <input type="submit" className="btn btn-primary" value="Invia" />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}