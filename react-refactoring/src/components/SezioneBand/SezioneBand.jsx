import React from "react";

const MembriBand = [
    { 
    ruolo: "Cantante", 
    immagine: "/immagini/cantante3.jpg", 
    nome: "Matthew Bellamy" 
    },

    { 
    ruolo: "Bassista", 
    immagine: "/immagini/bassista2.jpg", 
    nome: "Chris Wolstenholme" 
    },

    { 
    ruolo: "Batterista", 
    immagine: "/immagini/batterista2.jpg", 
    nome: "Dominic Howard" 
    }
];

export default function SezioneBand() {
    return (
        <section className="container" style={{ textAlign: "center", paddingTop: "3%", fontFamily: 'Georgia, "Times New Roman", Times, serif'}}>
            <h2 style={{ letterSpacing: "5px" }}>THE BAND</h2>
            <p className="paragrafo" style={{ fontSize: "18px" }}>
            And disciplinary remains mercifully Yes and um, I'm with you Derek, this star nonsense Yes, yes Now which is it? I am sure of it So, so you think you can 
            tell Heaven from hell? Blue skies from pain? Can you tell a green field From a cold steel rail? A smile from a veil? Do you think you can tell? Did they get
            you to trade Your heroes for ghosts? Hot ashes for trees? Hot air for a cool breeze? Cold comfort for change? Did you exchange A walk-on part in the war 
            For a lead role in a cage? How I wish, how I wish you were here We're just two lost souls Swimming in a fish bowl Year after year Running over the same old
            ground What have we found? The same old fears Wish you were here
            </p>

            <div className="display-section">
                {/* Utilizzo del metodo map per creare dinamicamente le card per ogni membro della band */}
                {MembriBand.map((membro, index) => (
                    <div className="card" key={index}>
                        <h3>{membro.ruolo}</h3>
                        <img className="card-flex" src={membro.immagine} alt={"Immagine " + membro.ruolo} />
                        <p>{membro.nome}</p>
                    </div>
                ))}
            </div>
        </section>
    );
}