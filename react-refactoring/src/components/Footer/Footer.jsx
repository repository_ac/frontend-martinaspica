import React from "react";

export default function Footer() {
    return (
        <footer style={{ fontFamily: 'Georgia, "Times New Roman", Times, serif' }}>
        <div className="container">
            <div className="row">
            <div className="col-md-6" style={{display: "flex", alignContent: "start", justifyContent: "start"}}>
                &copy; 2024 Copyright
            </div>
            <div className="col-md-6 text-right">
                <a href="#" style={{ color: "white", textDecoration: "none", display: "flex", alignContent: "end", justifyContent: "end" }}>Torna all'inizio </a>
            </div>
            </div>
        </div>
        </footer>
    );
}
